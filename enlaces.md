## Enlaces y referencias

- [Haskell Report](https://www.haskell.org/onlinereport/index.html)
- [Haskell Package Repository](https://hackage.haskell.org/)
- [Varios enlaces](https://www.haskell.org/documentation/)

## Bioinformática con haskell

- https://hackage.haskell.org/package/bio
- https://wiki.haskell.org/Applications_and_libraries/Bioinformatics
- https://github.com/PapenfussLab/bioshake
