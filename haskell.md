# Caracterísiticas de haskell

```{important}
Haskell es un lenguaje de programación funcional puro
```

## Características

- Las funciones no poseen efectos secundarios y el resultado de una función depende únicamente de sus argumentos.
- Evaluación perezosa - Lazy Evaluation: las expresiones no se evalúan hasta que es necesario.
- Inferencia de tipos: permite al compilador deducir el tipo de las expresiones.
- Las funciones son ciudadanos de primera clase, lo que significa que pueden ser pasadas como argumentos a otras funciones, devueltas como resultados y almacenadas en estructuras de datos.
- Recursión: la recursión es una técnica común,  y el lenguaje fomenta el uso de estructuras recursivas en lugar de bucles.
- Permite la definición de tipos algebraicos de datos, lo que facilita la creación de estructuras de datos complejas y expresivas.
- Las estructuras de datos en Haskell son inmutables por defecto.

### Lenguaje tipado - Declaración de tipos

La declaración de tipos en Haskell es una forma de decirle al compilador qué tipos de datos esperar para los argumentos de una función y qué tipo de resultado se debe esperar que devuelva. Es como especificar el tipo de información que una función maneja.

Cuando defines una función en Haskell, puedes incluir una declaración de tipo para indicar qué tipos de valores la función acepta como entrada y qué tipo de valor devuelve como resultado. Esto ayuda al compilador a verificar que estás utilizando la función de manera coherente y también proporciona información útil para otros programadores que puedan leer tu código.

Veamos un ejemplo:

Una función duplicar, que cuando le pasamos un número, la función devuelve el doble de ese número.
```
doble :: Int -> Int
doble x = x * 2
```
La primer línea, es la declaración de tipo y se lee de la siguiente forma:

>    doble: Es el nombre de la función.

>    :: : Se lee como "tiene el tipo".

>    Int -> Int: Significa que la función toma un entero como argumento (Int) y devuelve otro entero como resultado (Int).

```{tip}
La declaración de tipos de una función se conoce comúnmente como la "firma" o "tipo" de la función. 

La firma de una función especifica los tipos de sus argumentos y el tipo de su resultado. Esto ayuda a Haskell a inferir y verificar los tipos de manera segura durante la compilación y también sirve como documentación para los programadores.
```
