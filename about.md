# Mis notas del lenguaje de programación Haskell

<img src="leoMate.png" alt="Filosofando"  width="200px">

Si los lenguajes de programación fueran parte de la cordillera de los Andes, y yo fuera un escalador experimentado, Haskell sería el Aconcagua.

He escalado varias montañas, pero esta.. mamita querida. recién estoy en el primer refugio, recuperando fuerzas, preparando la ascensión. 

No importa los años programando, en lo que al lenguaje haskell se refiere, estoy al inicio. 

Hace apróximadamente dos años (2021) comencé a incursionar en el mundo de Haskell, al 
haber participado y completado la [segunda cohorte del programa Plutus Pioneers Programm](https://ipfs.io/ipfs/QmXy7ceoGMv6kq8H6KTcn13y6GQ4vtHxV1RRT5BGPfrcDr). 

Plutus Core, el lenguaje nativo de contratos inteligentes de Cardano, está escrito en Haskell. 

Los contratos de Plutus son efectivamente programas Haskell, por lo que era esencial para participar, conocer los conceptos básicos de Haskell.

Además de leer tutoriales y libros, en este momento, estoy continuando  mi formación en Haskell participando en una capacitación denominada **Cardano Haskell MOOC**, organizado  por la academia [Emurgo](https://education.emurgo.io/)

Comparto estas notas y textos a través de jupiter-book, herramienta que conocí durante unos workshop organizados por [BioNT Consortium](https://biont-training.eu/)
que escribo fundamentalmente para fijar y recordar los conocimientos adquiridos.


