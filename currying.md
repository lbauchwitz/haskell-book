# Currificación - Currying 

Ahora, en cuanto al currying, es un concepto relacionado con el cálculo lambda.

```{important}
El currying es una técnica que permite representar funciones de varios argumentos como una serie de funciones de un solo argumento.
```

Lo que hace esta técnica,  es transformar una función que toma varios argumentos en una secuencia de funciones que toman un solo argumento cada una. 

En lugar de proporcionar todos los argumentos de una vez, puedes aplicar la función parcialmente, proporcionando uno o más argumentos en etapas sucesivas.

Esta técnica facilita la creación de funciones más flexibles y componibles, permitiéndote construir nuevas funciones a partir de funciones existentes de manera más modular.

----


En lugar de tener una función que toma, por ejemplo, dos argumentos como (x, y), puedes representarla como una serie de funciones que se aplican secuencialmente, como f(x)(y).

En Haskell, el currying es una característica fundamental.

Las funciones en Haskell se currean/currian :) de forma automática.

Esto significa que cuando defines una función que parece tomar múltiples argumentos, en realidad estás definiendo una serie de funciones de un solo argumento que se aplican de forma encadenada.

```
-- Función original sin currying
suma :: Int -> Int -> Int
suma a b = a + b

-- Función curried
sumaCurried :: Int -> Int -> Int
sumaCurried a = \b -> a + b

-- Uso de la función curried
resultado :: Int
resultado = sumaCurried 3 5
```
1. Definimos una función original suma que toma dos argumentos.
1. Creamos una versión curried de la función suma llamada sumaCurried. Esta función toma un solo argumento a y devuelve una función que toma un segundo argumento b.
1. La función curried sumaCurried toma a y devuelve una función anónima (usando \b ->) que suma a y b.
1. Llamamos a la función curried proporcionando un valor para a y luego otro para b.
	
