# Trabajando con haskell

```{note}
GHCI se refiere al intérprete interactivo para el compilador de Haskell.
(Glasgow Haskell Compiler interactive)
```


- Ejecutar Emacs
- CTRL + x 3 Divide la pantalla en dos buffers
- En uno de los terminales, ejecutar `ALT + X` y escribir `shell`
- En el shell, escribir `ghci`, esto ejecuta el intérprete interactivo de Haskell.

```{tip}
Si en algún momento deseas salir del GHCI y volver al shell, puedes escribir el comando :quit
```

- En el otro buffer, cargar y trabajar sobre el archivo del programa. 
Supongamos que el archivo de programa en el que deseas trabar se llam apiBancaria.hs
- En el buffer de emacs, cargar dicho programa (CTRL + F)
- En el intérprete cargar dicho archivo mediante el comando `:l apiBancaria.hs 
- Mientras estas trabajando, puedes volver a cargar en forma automática el mismo archivo en GHCI mediante el comando `:r`
