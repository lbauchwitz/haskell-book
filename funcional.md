# Programación funcional

![Programación funcional](http://ferestrepoca.github.io/paradigmas-de-programacion/progfun/funcional_teoria/images/function.jpg)

```{important}
La esencia de la programación funcional, es que un programa es una secuencia de expresiones.


- Las expresiones pueden incluir, valores concretos, variables y/o funciones.
- En programación funcional, un programa se construye como una secuencia de expresiones. Cada expresión se evalúa para producir un valor. 
- Las expresiones pueden contener valores concretos, variables y llamadas a funciones. 
- Las funciones tienen una definición más específica. Son expresiones que son aplicadas a un argumento o una entrada, que puede ser reducida o evaluada. 
- Las funciones pueden asignarse a variables, pasarse como argumentos a otras funciones y devolverse como resultados de funciones. 
Esto permite una gran flexibilidad en el diseño y composición de programas.


> 💡 **Programación Funcional:**
>
> Las funciones puras son como las mejores amistades: confiables, predecibles y no te cambian el estado de ánimo.
