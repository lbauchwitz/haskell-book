# Cálculo Lambda

El cálculo lambda es un sistema formal y matemático que se utiliza para representar y estudiar funciones y la aplicación de funciones en la teoría de la computación y la programación.

```{admonition} Un cacho de cultura
Fue desarrollado por [Alonzo Church](https://es.wikipedia.org/wiki/Alonzo_Church) en la década de 1930 y es la base teórica de muchos lenguajes de programación funcionales, incluido Haskell.
```
En términos sencillos, el cálculo lambda se centra en la idea de funciones anónimas o sin nombre. En lugar de definir una función con un nombre como lo harías en lenguajes de programación más convencionales, en el cálculo lambda, defines funciones directamente utilizando la notación λ (lambda).

La sintaxis básica del cálculo lambda es la siguiente:

```
λ parámetro . cuerpo
```

* λ representa la letra griega lambda y se usa para denotar que estás definiendo una función.
* parámetro es el argumento o variable que la función toma como entrada.
* cuerpo es la expresión que describe lo que la función hace con ese argumento.

Por ejemplo, aquí hay una función lambda que suma 2 a su argumento:
```
λ x . x + 2
```

## Anónimas

En el contexto del cálculo lambda, las funciones son consideradas anónimas porque no están asociadas a un nombre específico. En lugar de definir funciones con nombres, como se hace en muchos lenguajes de programación, el cálculo lambda se centra en la abstracción de funciones sin asignarles un identificador.

En el cálculo lambda, una función se representa de la siguiente manera:

λparámetro.cuerpo de la función

Por ejemplo:

`λx.x+2`

```note
    λ: Indica que estás definiendo una función.
    x: Es el parámetro de la función.
    .: Separa el parámetro del cuerpo de la función.
    x+2: Es el cuerpo de la función, donde se describe la operación a realizar con el parámetro x.
```	

Por ejemplo, una función lambda que suma dos a un argumento dado se expresaría como:

`λx.x+2`

Esta función toma un argumento x y devuelve el resultado de sumarle 2. 

```{important}
La clave es que no hay un nombre asociado a esta función, es simplemente una abstracción que representa la operación de sumar 2 a su entrada.
```

## Notación funciones anónimas



```{tip}
En Haskell, el carácter \ se utiliza para representar funciones anónimas mediante lo que se conoce como "lambda abstracciones". 
```
Esta notación es consistente con el cálculo lambda y es una forma de crear funciones sin asignarles un nombre.

En Haskell, una lambda abstracción se define de la siguiente manera:

`\parameter -> body`

- \: Indica que estás definiendo una función anónima.
- parameter: Es el parámetro de la función.
- ->: Separa el parámetro del cuerpo de la función.
- body: Es el cuerpo de la función, donde se describe qué hacer con el parámetro.

## Ejemplo:

```
\x -> x + 2
```
Indica que estamos definiendo una función anónima que toma un argumento llamado x, 

x + 2 es el cuerpo de la función. 

Esta función toma un argumento x y devuelve el resultado de sumar 2 a ese argumento.
