# Abstracción

En Haskell, la abstracción se refiere a la capacidad de representar conceptos y operaciones de una manera general y abstracta, lo que permite escribir código genérico que puede funcionar con diferentes tipos de datos sin conocer los detalles específicos de esos tipos. 


Uno de los conceptos fundamentales en Haskell es la abstracción de tipos, que se logra a través de tipos de datos algebraicos y typeclasses. 



> 💡 **Sin dormir**
>
> ¿Por qué la abstracción y el mate son tan parecidos? 
>
> Ambos te mantienen despierto por la noche reflexionando acerca de cosas que no comprendes completamente. 
>
> <img src="mate.jpg" alt="Mate amargo y reflexión"  width="200px">

