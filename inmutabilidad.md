# Inmutabilidad

La inmutabilidad  implica que una vez que se crea un objeto (como una variable o estructura de datos),  su estado no puede ser modificado. 

En lugar de realizar cambios directos en los datos existentes, se favorece la creación de nuevos datos con base en los existentes. 

La inmutabilidad implica que, en lugar de modificar directamente un estado existente, se crea un nuevo estado basado en el estado anterior. 


Este enfoque tiene realmente, implicaciones significativas para la claridad del código, la concurrencia y la capacidad de razonar sobre el programa.

---------
## En la vida real hay cambios de estado. 

Sin embargo, en la vida real, todo transcurre dinámicamente, muta cambia. Situaciones comunes en la vida diaria, donde las cosas no permanecen estáticas y pueden evolucionar o transformarse. Un ejemplo son los saldos de cuentas bancarias cambian debido a depósitos, retiros o intereses acumulados.

A fin de cuentas, la forma en que representamos la realidad en nuestros programas (el modelo que elegimos) puede ser distinto.
En el caso del saldo de la cuenta bancaria, la implementación del saldo en nuestro programa, podría ser a través de una variable llamado Saldo que cambia con el transcurso del tiempo. Pero también podríamos implementar el mismo, a través de una función, cuya salida sea el saldo, resultante de calcular la diferencia entre las entradas y las salidas.


```{note}
En la programación imperativa,se tiende a modificar el estado de las variables de manera directa.

En la programación funcional, se enfoca en la evaluación de funciones y se evita la modificación directa del estado de las variables.
```

Son dos enfoques diferentes. Dos alternativas, cada uno con sus ventajas y desventajas. 
(reflexionando)=
### Reflexionando

- ¿Cómo representamos los cambios de estado, si adoptamos como estrategia, que los objetos sean inmutables?
- ¿Cómo mantenemos un registro de los distintos estados, y, al mismo tiempo, conocer cual es el estado actual en un momento determinado?
- ¿Qué consecuencias presenta un enfoque funcional, en escenarios concurrentes?



### Analogías - pensando en clones

Este proceso es análogo a la idea de generar "clones" de uno mismo en diferentes estados. Dada la imposibilidad de cambiar mi propio ADN, cada versión o clon representaría un nuevo estado que incorpore las modificaciones deseadas o necesarias en el genoma.


### Git, commits y estados
Pensando en un caso más concreto, la  inmutabilidad se asemeja a cómo funcionan los sistemas de control de versiones como Git. Y puede ser interesante, utilizarlo como marco de referencia al implementar soluciones en programación funcional. 

En Git, cada cambio en un proyecto genera una nueva "instantánea" o "commit". 

Cada commit representa un estado específico del código en un momento dado. En lugar de modificar directamente los archivos existentes, Git crea una nueva versión que refleja los cambios realizados.

Los commits en Git son inmutables y conservan la integridad del historial del proyecto. Puedes retroceder en el tiempo y revisar cualquier estado anterior del código. Este enfoque es coherente con la idea de inmutabilidad en programación funcional, donde cada cambio crea una nueva versión independiente sin afectar las versiones anteriores.

## Cambio de estado - Ejemplo subasta

En una subasta, el precio de un bien específico está sujeto a cambios continuos debido a la presentación de nuevas ofertas. Por lo menos hasta que finaliza la subastas.
Abordar este problema en el contexto de la programación funcional, implica  el desafío de cómo podemos manejar eficazmente estos cambios de estado sin comprometer los principios de inmutabilidad.

Este escenario refleja la naturaleza dinámica de muchas situaciones del mundo real, donde el estado de un objeto o entidad se modifica con el tiempo.

La inmutabilidad postula que una vez que se ha creado un estado o valor, este no puede ser alterado directamente. En lugar de modificar un precio existente, se favorece la creación de nuevos estados basados en la información previa. En el caso de la subasta, esto significa que, en lugar de actualizar un precio en su lugar, creamos un historial de precios que refleja cada nueva oferta.

Cada oferta- puja implicaría la creación de  un nuevo estado, formando un historial completo de las mismas.  Y esto facilitaría el rastreo de eventos pasados, en este caso conocer el valor  de cada puja en particular, en un momento en particular.

Me vuelvo a planter las mismas preguntas formuladas previamente ({ref}`reflexionando`) a esta situación en particular.

- ¿Cómo representamos los cambios del precio, si adoptamos como estrategia, que el precio es inmutable?
- ¿Cómo mantenemos un registro de las distintas pujas en el tiempo, y, al mismo tiempo, permitimos que el sistema indique cual es el último  precio?
- ¿Qué consecuencias presenta un enfoque funcional, en escenarios concurrentes, si dos compradores pujan al mismo tiempo?

La implementación práctica podría implicar estructuras de datos inmutables que registren cada oferta y su precio asociado en un orden cronológico. De esta manera, tenemos un historial completo de cambios de estado sin perder la capacidad de identificar rápidamente el precio más reciente. Este enfoque no solo facilita el rastreo de eventos pasados sino que también proporciona un marco robusto para el análisis de datos y la toma de decisiones basada en el historial completo de precios.

En lugar de tener una única variable mutable que almacene el último precio, podríamos tener una estructura de datos, como una lista o una secuencia, que almacene todos los precios anteriores. Cada vez que se presenta una nueva oferta, se crea una nueva estructura de datos que incluye el nuevo precio, manteniendo así un historial de todos los cambios.

## Concurrencia

La inmutabilidad es mostrada además crucial  en la escritura de programas concurrentes y paralelos. 
Parecería que al evitar que varios hilos o procesos modifiquen el mismo dato al mismo tiempo, se eliminarían los problemas de concurrencia, y la necesidad de mecanismos de bloqueo complicados. 



